/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basededatos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;

/**
 *
 * @author BAPA
 */
public class JavaPostgreSQLBasic {
    Connection connection;
    PreparedStatement stmt, stmt2;
     public void connectDatabase() {
        try {
            // We register the PostgreSQL driver
            // Registramos el driver de PostgresSQL
            try { 
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                System.out.println("Error al registrar el driver de PostgreSQL: " + ex);
            }
            connection = null;
            // Database connect
            // Conectamos con la base de datos
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost/byron",
                    "openpg", "openpgpwd");
                    //"jdbc:postgresql://172.30.102.28:5432/BasePrueba",
                    //"facci", "facci2019");
            boolean valid = connection.isValid(50000);
            if(valid == true){
                System.out.println("Correcto");
                int principal = 100;
                int detalle = 1;
                for (int i = 0; i < 101; i++) {
                    InsertarPrincipal(principal++);
                }
                int principal2 = 100;
                for (int j = 0; j < 101; j++) {
                        InsertarDetalle(detalle++, principal2++);
                    }
                    
                
            }else{
                System.out.println("fallo");
            }
            //System.out.println(valid ? "TEST OK" : "TEST FAIL");
            
        } catch (java.sql.SQLException sqle) {
            System.out.println("Error: " + sqle);
        }
    } 
     
    private void InsertarPrincipal(int id){
        try {
            stmt = connection.prepareStatement("INSERT INTO principal (id, descripcion, idusuario, total) VALUES (?,?,?,?)");
            stmt.setInt(1, id);
            stmt.setString(2, "Registro" + id);
            stmt.setInt(3, id+5);
            stmt.setInt(4, id+10);
            stmt.execute();
            System.out.println("Dato " + id + " Insertado En Tabla Principal");
        } catch (SQLException ex) {
            Logger.getLogger(JavaPostgreSQLBasic.class.getName()).log(Level.SEVERE, null, ex);
        }
               
                
    }

    private void InsertarDetalle(int id, int idP){
       
        try {
            stmt2 = connection.prepareStatement("INSERT INTO detalle (id, principal_id, clavepredial, descripcion, subtotal, cantidad) VALUES (?,?,?,?,?,?)");
            stmt2.setInt(1, id);
            stmt2.setInt(2, idP);
            stmt2.setString(3, "Registro" + id);
            stmt2.setString(4, "132456789-0");
            stmt2.setInt(5, id+20);
            stmt2.setInt(6, id+50);
            stmt2.execute();
            System.out.println("Dato " + id + " Insertado En Tabla Detalle");
        } catch (SQLException ex) {
            Logger.getLogger(JavaPostgreSQLBasic.class.getName()).log(Level.SEVERE, null, ex);
        }  
    }
    
}   